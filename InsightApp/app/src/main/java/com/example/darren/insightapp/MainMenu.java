package com.example.darren.insightapp;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainMenu extends Activity {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainmenu);

        // Hides the action bar that would usual be at the top of the layout
        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }

        Button Resume_Exercises = (Button) findViewById(R.id.Resume_Exercises);
        Button Revise_Exercises = (Button) findViewById(R.id.Revise_Exercises);
        Button SensorDebugbtn = (Button) findViewById(R.id.SensorDebugbtn);

        // Checking if button was clicked...
        Resume_Exercises.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent main = new Intent(getApplicationContext(), CurrentExercise.class);
                startActivity(main);
            }
        });

        Revise_Exercises.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent main = new Intent(getApplicationContext(), Revise_exercise.class);
                startActivity(main);
            }
        });

        SensorDebugbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent main = new Intent(getApplicationContext(), SensorData.class);
                startActivity(main);
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent setIntent = new Intent(MainMenu.this, Login.class);
        startActivity(setIntent);
    }
}
