package com.example.darren.insightapp;

import android.app.ActionBar;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Button;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;

public class CurrentExercise extends Activity {

    int angle = 0;
    long timer_lenght = 0;
    int totalseconds = 0;
    int exercise = 0;

    Timer mycounter;
    ImageView focus_point;
    TextView Exercise_No;
    TextView Speed;
    SeekBar slider;
    TextView timer;
    Button RotateBtn;
    Button InstructBtn;
    TextView test;

    Handler handler;
    Runnable counter;

    View hiddenInfo;
    PopupWindow popupWindow;

    BluetoothAdapter bluetoothAdapter;


    // These two arrays hold information about the different pixel sizes the focus point can be and the log value of these sizes
    float[] fontsize = {9.9969913803f, 12.5854665423f, 15.8441637534f, 19.9466205854f, 25.1113079486f, 31.6132645265f, 39.7987437114f, 50.1036531064f, 63.0767686849f};
    float[] logMAR = {-0.3f, -0.2f, -0.1f, 0f, 0.1f, 0.2f, 0.3f, 0.4f, 0.5f};

    // Holds a list of exercises
    List<Exercise_properties> exerc = new ArrayList<Exercise_properties>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.current_exercise);

        // Hides the action bar that would usual be at the top of the layout
        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }

        // Bluetooth
        bluetooth_start();

        // A layout used as a container to hold the exercise in the middle of the screen
        // eg could be holding a single focus point or multiple moving focus points depending on the exercise
        RelativeLayout hiddenLayout = (RelativeLayout)findViewById(R.id.relativeLayout);

        // Selected exercise to be placed in the hiddenLayout container
        hiddenInfo = getLayoutInflater().inflate(R.layout.exercise1, hiddenLayout, false);
        hiddenLayout.addView(hiddenInfo);

        //Declaring components from the xml layout (currently called current_exercisercise.xml)
        focus_point = (ImageView) findViewById(R.id.focus_point);
        Speed = (TextView) findViewById(R.id.Speed_txt);
        Exercise_No = (TextView) findViewById(R.id.ExerciseNo_txt);
        slider = (SeekBar) findViewById(R.id.seekBar);
        timer = (TextView) findViewById(R.id.timer_txt);
        RotateBtn = (Button) findViewById(R.id.rotateBtn);
        test = (TextView) findViewById(R.id.test);

        exercises();            //Adds the exercises to the list
        settingup_exercise();   //Sets up the individual properties of the exercise and starts the Timer


        int starting_size = (int) (fontsize[0] * pixel_density());      // Starting size is the smallest the focus point can be and the first position in the array
        focus_point.getLayoutParams().height=starting_size;           // Sets the height of the focus point image to the smallest size (square)
        focus_point.getLayoutParams().width=starting_size;            // Sets the width of the focus point image to the smallest size

        open_Popup();

        slider.setMax(8);           // Sets the max value of the seekbar to 8, the value iterates by 1
        slider.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
        int progressChanged = 0;     // Initialised seekbar to 0

        @Override
        public void onProgressChanged (SeekBar seekBar,int progress, boolean fromUser)
        {
            progressChanged = progress;
            int size = (int) (fontsize[progress] * pixel_density());     // Gets the font size depending on the seekbar position multiplied by pixel density difference (see Function 'pixel_density')
            hiddenInfo.findViewById(R.id.focus_point).getLayoutParams().height = size;                // Sets the height of the focus point image to size (square)
            hiddenInfo.findViewById(R.id.focus_point).getLayoutParams().width = size;                 // Sets the width of the focus point image to size
            hiddenInfo.forceLayout();       // Forces the view to update the layout
            }

            @Override
            public void onStartTrackingTouch (SeekBar seekBar){
            }

            @Override
            public void onStopTrackingTouch (SeekBar seekBar){
                // Popup Message, displaying LogMar value of focus point
                Toast.makeText(CurrentExercise.this, "LogMAR Value: " + logMAR[progressChanged], Toast.LENGTH_SHORT).show();
            }
        });

       RotateBtn.setOnClickListener(new View.OnClickListener() {   // Action to be run if the Rotation button is pressed
            @Override
            public void onClick(View v) {
                if (angle == 360)   // If the angle equals 360
                {
                    angle = 0;      // Resets the angle of rotation back to 0
                }
                angle += 90;        // Increases the Angle of Rotation by 90 degrees
                focus_point.setRotation(angle);     // Rotates the focus point by the value of the angle variable
            }
        });
    }

        @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }

    float pixel_density() {
        // Source of focus point size
        // Sample width of screen in mm     : 280
        // Sample width of screen in pixels : 1280

        float inches = 280 / 25.4f; // There is 25.4 mm in an inch
        float xdpi = 1280 / inches; // How many pixels per inch (Over the width of the screen)

        float device_xdpi = getResources().getDisplayMetrics().xdpi;  // Gets the devices xdpi ( The number of dots(pixels) per inch over the width of the device )
        return device_xdpi / xdpi;    // Returns the difference between sample device and actual device dpi
    }

    private void exercises() {
        exerc.add(new Exercise_properties(10, "White", 0.3f));
        exerc.add(new Exercise_properties(10, "Gold", 0.4f));
        exerc.add(new Exercise_properties(10, "LightBlue", 0.5f));
        exerc.add(new Exercise_properties(10, "LightGray", 0.6f));
        exerc.add(new Exercise_properties(10, "LightGray", 0.7f));
    }

    private void settingup_exercise() {
        Exercise_No.setText("Exercise No : " + (exercise + 1));          // Sets the Exercise_No text box as the current exercise number
        Speed.setText("Speed : " + exerc.get(exercise).speed + "Hz");   // Sets the Speed text box as the speed of head movement of the exercise
        timer_lenght = exerc.get(exercise).timer * 1000;                       // Gets the time of the exercise
        mycounter = new Timer();
        mycounter.setDuration(timer_lenght);
        RefreshTimer();
        mycounter.Start();

    }

    //runs without a timer by reposting this handler at the end of the runnable
    public void RefreshTimer()
    {
        handler = new Handler();
        counter = new Runnable(){

            public void run(){
                totalseconds = mycounter.getCurrentTime();                  //Gets the remaining time left on the timer in seconds
                int minutes = totalseconds / 60;                            // Breaks the number of seconds down into minutes
                int seconds = totalseconds % 60;                            // Gets the remaining seconds excluding the minutes
                timer.setText(String.format("Remaining %d:%02d", minutes, seconds));        // Updates the timer text box
                handler.postDelayed(this, 100);

                //if ((mycounter.getCurrentTime() == 0) && (exercise < 4))
                //{
                //    exercise++;
                //    settingup_exercise();
                //}
                if (mycounter.getCurrentTime() == 0)
                {
                    changeactivity();
                }
            }
        };
        handler.postDelayed(counter, 100);
    }

    void changeactivity()           // Open dizziness Scale activity
    {
        handler.removeCallbacks(counter);      // Stops timer
        Intent intent = new Intent(CurrentExercise.this, Dizziness_scale.class);
        startActivity(intent);
    }
    void open_Popup(){
        mycounter.Stop();
        LayoutInflater inflater = (LayoutInflater)getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupview = inflater.inflate(R.layout.instructions, null);
        popupWindow= new PopupWindow(popupview, WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.MATCH_PARENT );
        InstructBtn = (Button) popupview.findViewById(R.id.instructions_close);
        InstructBtn.setOnClickListener(new View.OnClickListener() {     // If the button at the bottom of the popup is pressed
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();                  // Closes popup window
                mycounter.Start();
            }
        });

        popupview.post(new Runnable() {                 // Waits until the activity window has been displayed before loading the popup windows
            public void run() {
                popupWindow.showAtLocation(findViewById(R.id.act), 0, 0, 20);  // Displays Popup window over the current_exercise layout, the last parameter is 20 pixels from the top of the screen
            }
        });
    }

    @Override
    public void onBackPressed() {
        mycounter.Stop();
        popupWindow.dismiss();
        finish();
        Intent setIntent = new Intent(CurrentExercise.this, MainMenu.class);
        startActivity(setIntent);
    }

    public void bluetooth_start(){
        // Bluetooth
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // Ensures Bluetooth is available on the device and it is enabled. If not,
        // force enables Bluetooth.

        if (bluetoothAdapter == null)
        {
            Toast.makeText(getApplicationContext(), "Device does not have bluetooth", Toast.LENGTH_SHORT).show();
            finish();
        }
        else if (!bluetoothAdapter.isEnabled()) {
            // Force Enable Bluetooth
            bluetoothAdapter.enable();
            Toast.makeText(getApplicationContext(), "Bluetooth Enabled", Toast.LENGTH_SHORT).show();
        }
    }
}
