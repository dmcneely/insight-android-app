package com.example.darren.insightapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBManager {
    public static final String KEY_USERID = "_id";
    public static final String KEY_FNAME = "firstname";
    public static final String KEY_SNAME = "secondname";
    public static final String KEY_ADDRESS = "address";
    public static final String KEY_DOB = "dob";
    public static final String KEY_Email = "email";
    public static final String KEY_PASSWORD = "pass";

    public static final String KEY_EXERCISENO = "exercise";
    public static final String KEY_DAY = "day";
    public static final String KEY_DATE = "date";
    public static final String KEY_COMP = "completed";
    public static final String KEY_DUR = "duration";

    private static final String DATABASE_NAME = "Database";
    private static final String DATABASE_TABLE1 = "Users";
    private static final String DATABASE_TABLE2 = "Claims";
    private static final int DATABASE_VERSION = 1;


    private DatabaseHelper DBHelper;
    private SQLiteDatabase db;

	/* Database class handles all the queries */

    public DBManager(Context ctx) {
        DBHelper = new DatabaseHelper(ctx);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            // Creating tables
            String DATA1_CREATE = "create table " + DATABASE_TABLE1 + "(_id integer primary key autoincrement, "
                    + "firstname text not null, "
                    + "secondname text not null,"
                    + "address text not null,"
                    + "dob text not null,"
                    + "email text not null,"
                    + "pass text not null);";

            String DATA2_CREATE = "create table " + DATABASE_TABLE2 + "(exercise integer, "
                    + "day integer, "
                    + "date date, "
                    + "completed text, "
                    + "duration integer);";

            db.execSQL(DATA1_CREATE);
            db.execSQL(DATA2_CREATE);

            //default inserts
            db.execSQL("INSERT INTO " + DATABASE_TABLE1 + " (firstname, secondname, address, dob, email, pass) Values ('Darren' , 'McNeely' , '13 MapleDrive', '20_05_93', '', '')");

            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (0, 0, '2003-12-31', 'available', 60)");
            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (1, 0, '2003-12-31', 'available', 60)");
            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (2, 0, '2003-12-31', 'available', 60)");
            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (3, 0, '2003-12-31', 'available', 60)");
            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (4, 0, '2003-12-31', 'available', 60)");

            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (0, 1, '2003-12-31', 'available', 60)");
            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (1, 1, '2003-12-31', 'available', 60)");
            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (2, 1, '2003-12-31', 'available', 60)");
            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (3, 1, '2003-12-31', 'available', 60)");
            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (4, 1, '2003-12-31', 'available', 60)");

            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (0, 2, '2003-12-31', 'available', 60)");
            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (1, 2, '2003-12-31', 'available', 60)");
            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (2, 2, '2003-12-31', 'available', 60)");
            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (3, 2, '2003-12-31', 'available', 60)");
            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (4, 2, '2003-12-31', 'available', 60)");

            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (0, 3, '2003-12-31', 'available', 60)");
            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (1, 3, '2003-12-31', 'available', 60)");
            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (2, 3, '2003-12-31', 'available', 60)");
            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (3, 3, '2003-12-31', 'available', 60)");
            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (4, 3, '2003-12-31', 'available', 60)");

            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (0, 4, '2003-12-31', 'available', 60)");
            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (1, 4, '2003-12-31', 'available', 60)");
            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (2, 4, '2003-12-31', 'available', 60)");
            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (3, 4, '2003-12-31', 'available', 60)");
            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (4, 4, '2003-12-31', 'available', 60)");

            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (0, 5, '2003-12-31', 'available', 60)");
            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (1, 5, '2003-12-31', 'available', 60)");
            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (2, 5, '2003-12-31', 'available', 60)");
            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (3, 5, '2003-12-31', 'available', 60)");
            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (4, 5, '2003-12-31', 'available', 60)");

            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (0, 6, '2003-12-31', 'available', 60)");
            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (1, 6, '2003-12-31', 'available', 60)");
            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (2, 6, '2003-12-31', 'available', 60)");
            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (3, 6, '2003-12-31', 'available', 60)");
            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (4, 6, '2003-12-31', 'available', 60)");

            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (0, 7, '2003-12-31', 'available', 60)");
            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (1, 7, '2003-12-31', 'available', 60)");
            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (2, 7, '2003-12-31', 'available', 60)");
            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (3, 7, '2003-12-31', 'available', 60)");
            db.execSQL("INSERT INTO " + DATABASE_TABLE2 + " (exercise, day, date, completed, duration) Values (4, 7, '2003-12-31', 'available', 60)");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // drops table if it- exists
            db.execSQL("DROP TABLE IF EXISTS" + DATABASE_TABLE1);

            // create fresh books table
            this.onCreate(db);
        }
    }

    public DBManager open() throws SQLException {
        db = DBHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        DBHelper.close();
    }

    public long insertUser(String firstname, String secondname, String address, String dob, String email, String pass) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_FNAME, firstname);
        initialValues.put(KEY_SNAME, secondname);
        initialValues.put(KEY_ADDRESS, address);
        initialValues.put(KEY_DOB, dob);
        initialValues.put(KEY_Email, email);
        initialValues.put(KEY_PASSWORD, pass);
        return db.insert(DATABASE_TABLE1, null, initialValues);
    }


    public Cursor getAllExercises(Integer exerciseno, Integer day) {
        return db.query(DATABASE_TABLE2, new String[]{
                        KEY_EXERCISENO,
                        KEY_DAY,
                        KEY_DATE,
                        KEY_COMP,
                        KEY_DUR},
                null,
                null,
                null,
                null,
                null
        );
    }

    public Cursor getUser(String rowId) throws SQLException {
        Cursor mCursor =
                db.query(true, DATABASE_TABLE1, new String[]{
                                KEY_USERID,
                                KEY_FNAME,
                                KEY_SNAME,
                                KEY_ADDRESS,
                                KEY_DOB,
                                KEY_Email,
                                KEY_PASSWORD
                        },
                        KEY_USERID + "='" + rowId + "'",
                        null,
                        null,
                        null,
                        null,
                        null
                );

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    public boolean getUser(String email, String pass) throws SQLException {
        Cursor mCursor =
                db.query(true, DATABASE_TABLE1, new String[]{
                                KEY_USERID,
                                KEY_FNAME,
                                KEY_SNAME,
                                KEY_ADDRESS,
                                KEY_DOB,
                                KEY_Email,
                                KEY_PASSWORD
                        },
                        KEY_Email + "='" + email + "'" + " and " + KEY_PASSWORD + "='" + pass + "'",
                        null,
                        null,
                        null,
                        null,
                        null
                );

        if (mCursor != null) {
            mCursor.moveToFirst();
        }

        if (mCursor.getCount() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
