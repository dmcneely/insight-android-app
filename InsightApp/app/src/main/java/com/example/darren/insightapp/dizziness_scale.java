package com.example.darren.insightapp;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.Calendar;

public class Dizziness_scale extends Activity {


    Button submit;
    TextView dizziness_date;
    SeekBar dizziness_seekBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dizziness_scale);

        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }

        dizziness_date = (TextView) findViewById(R.id.dizziness_date);
        dizziness_seekBar = (SeekBar) findViewById(R.id.dizziness_seekBar);
        submit = (Button) findViewById(R.id.dizziness_submit);
        submit.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                // Perform action on click
                Intent intent = new Intent(Dizziness_scale.this, CurrentExercise.class);
                startActivity(intent);
            }
        });

        Calendar rightNow = Calendar.getInstance();
        dizziness_date.setText(rightNow.get(Calendar.DAY_OF_MONTH) + "/" + (rightNow.get(Calendar.MONTH)+1) + "/" + rightNow.get(Calendar.YEAR));

        dizziness_seekBar.setMax(10);
        dizziness_seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged (SeekBar seekBar,int progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch (SeekBar seekBar){
            }

            @Override
            public void onStopTrackingTouch (SeekBar seekBar){
            }
        });

    }
}
