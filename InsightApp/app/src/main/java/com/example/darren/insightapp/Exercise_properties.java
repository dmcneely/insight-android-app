package com.example.darren.insightapp;

public class Exercise_properties
{
        int timer;
        String colour;
        float speed;

        public Exercise_properties(int timer, String colour, float speed)
        {
            this.timer = timer;
            this.colour = colour;
            this.speed = speed;

        }

        public String getbackground()
        {
        return colour;
        }

        public float getspeed()
        {
            return speed;
        }

        public int gettimer()
        {
        return timer;
        }
}
